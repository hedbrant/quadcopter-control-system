/*
 * motor_control.h
 *
 * Created: 2020-04-03 19:22:19
 *  Author: Olle
 */ 

#ifndef PWM_H_
#define PWM_H_

#define RL 1
#define RR 2
#define FR 3
#define FL 4

void pwm_init();
void pwm_set(uint8_t motor, uint8_t duty);
void pwm_set_all(uint8_t duty);

#endif /* PWM_H_ */