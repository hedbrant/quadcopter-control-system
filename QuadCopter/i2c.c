/*
 * i2c.c
 *
 * Created: 2020-04-02 23:26:49
 *  Author: Olle
 */ 

#include "QC_incl.h"

void i2c_init(){
	//Set Baud rate
	TWBR = 0x02;		//Baud rate 200 kHz for CPU freq 4 MHz
	TWCR |= (1 << TWIE);
}

//
void i2c_send_8_bit_data(uint8_t reg_addr, uint8_t data){
	//Send start condition & wait for it to be active
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);	//Send start condition
	while (!(TWCR & (1<<TWINT)));			//Wait for start condition to be true
	if ((TWSR & 0xF8) != I2C_START_COND){
		//_i2c_error(0);
	}
	
	//Sends out the address & a write condition
	TWDR = (SLA_MPU6050 << 1) | (I2C_W & 0xFF);
	TWCR = ((1 << TWINT) | (1 << TWEN)) & ~((1 << TWSTA) | (1 << TWSTO));
	
	//Checks if address & write was acknowledged
	while (!(TWCR & (1<<TWINT)));
	switch (TWSR){
		case I2C_MT_ACK_REC:
		break;
		case I2C_MT_NACK_REC:
		break;
		case I2C_MT_ARB_LOST:
		break;
		default:
		break;
	}
	
	//Specifies the register address by sending the address as a data byte
	TWDR = reg_addr;
	TWCR = ((1 << TWINT) | (1 << TWEN)) & ~((1 << TWSTA) | (1 << TWSTO));
	
	//Sends the data (repeat this section if multiple data bytes)
	TWDR = data;
	TWCR = ((1 << TWINT) | (1 << TWEN)) & ~((1 << TWSTA) | (1 << TWSTO));
	
	//Sends stop condition
	TWCR = ((1 << TWINT) | (1 << TWEN) | (1 << TWSTO)) & ~((1 << TWSTA));
}

uint8_t i2c_read_8_bit_data(uint8_t reg_addr){
	//Send start condition & wait for it to be active
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);	//Send start condition
	while (!(TWCR & (1<<TWINT)));			//Wait for start condition to be true
	if ((TWSR & 0xF8) != I2C_START_COND){
		//_i2c_error(0);
	}
	
	//Sends out the address & a read condition
	TWDR = (SLA_MPU6050 << 1) | (I2C_R & 0xFF);
	TWCR = ((1 << TWINT) | (1 << TWEN)) & ~((1 << TWSTA) | (1 << TWSTO));
	
	//Checks if address & write was acknowledged
	while (!(TWCR & (1<<TWINT)));
	switch (TWSR){
		case I2C_MR_ACK_REC:
		break;
		break;
		case I2C_MR_NACK_REC:
		break;
		case I2C_MT_ARB_LOST:
		break;
		default:
		break;
	}
	while (!(TWCR & (1<<TWINT)));
	return TWDR;
}

void _i2c_error(uint8_t err_code){
	/*switch (err_code){
		case 1:
		break;
		case 2:
		break;
		default:
		return;
		break;
	}*/
}