/*
 * IMU.h
 *
 * Created: 2020-02-24 21:51:40
 *  Author: Olle
 */ 

#ifndef IMU_H_
#define IMU_H_

#define SLA_MPU6050 0b1101000
#define IMU_INT_CONF_REG 0x37
#define IMU_GYRO_CONF_REG 0x1B
#define IMU_ACCEL_CONF_REG 0x1C
#define IMU_SAMP_RATE_DIV_REG 0x19

void imu_init();

#endif /* IMU_H_ */