/*
 * QC_incl.h
 *
 * Created: 2020-02-24 21:50:37
 *  Author: Olle
 */ 


#ifndef QC_INCL_H_
#define QC_INCL_H_

#define F_CPU 4000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <util/delay.h>
#include "system.h"
#include "LED.h"
#include "PID.h"
#include "motor_control.h"
#include "IMU.h"
#include "i2c.h"

#endif /* QC_INCL_H_ */