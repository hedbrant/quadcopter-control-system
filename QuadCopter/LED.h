/*
 * LED.h
 *
 * Created: 2020-04-03 19:25:37
 *  Author: Olle
 */ 

#ifndef LED_H_
#define LED_H_

#define LED1 1
#define LED2 2
#define LED3 3
#define LED4 4

void led_init();
void _init_sequence();
void set_led(uint8_t led, bool state);
void toggle_led(uint8_t led);

#endif /* LED_H_ */