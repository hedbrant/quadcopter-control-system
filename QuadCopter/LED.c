/*
 * LED.c
 *
 * Created: 2020-04-03 19:25:28
 *  Author: Olle
 */ 

#include "QC_incl.h"

bool led_state[4];

void led_init(){
	DDRD |= (1 << PD2);
	DDRC |= (1 << PC1);
	DDRC |= (1 << PC2);
	DDRC |= (1 << PC3);
	for (uint8_t i = 0; i < 4; i++){
		led_state[i] = false;
	}
	_init_sequence();
}

void _init_sequence(){
	set_led(LED1, true);
	_delay_ms(100);
	set_led(LED1, false);
	set_led(LED2, true);
	_delay_ms(100);
	set_led(LED2, false);
	set_led(LED3, true);
	_delay_ms(100);
	set_led(LED3, false);
	set_led(LED4, true);
	_delay_ms(100);
	set_led(LED4, false);
	set_led(LED1, true);
}

void set_led(uint8_t led, bool state){
	switch(led){
		case LED1:
		if(state){
			PORTD |= (1 << PD2);
		} else {
			PORTD &= ~(1 << PD2);
		}
		led_state[LED1 - 1] = state;
		break;
		
		case LED2:
		if(state){
			PORTC |= (1 << PC3);
		} else {
			PORTC &= ~(1 << PC3);
		}
		
		led_state[LED2 - 1] = state;
		break;
		
		case LED3:
		if(state){
			PORTC |= (1 << PC2);
		} else {
			PORTC &= ~(1 << PC2);
		}
		led_state[LED3 - 1] = state;
		break;
		
		case LED4:
		if(state){
			PORTC |= (1 << PC1);
		} else {
			PORTC &= ~(1 << PC1);
		}
		
		led_state[LED4 - 1] = state;
		break;
		
		default:
		return;
	}
}

void toggle_led(uint8_t led){
	set_led(led, !led_state[led - 1]);
}