/*
 * CFile1.c
 *
 * Created: 2020-02-24 21:49:34
 *  Author: Olle
 */ 

#include "QC_incl.h"

#define ROLL_KP 1
#define ROLL_KI 1
#define ROLL_KD 1
#define ROLL_LB -10
#define ROLL_UB 10
#define PITCH_KP 1
#define PITCH_KI 1
#define PITCH_KD 1
#define PITCH_LB -10
#define PITCH_UB 10
#define YAW_KP 1
#define YAW_KI 1
#define YAW_KD 1
#define YAW_LB -10
#define YAW_UB 10


struct pid {
	uint8_t kp;
	uint8_t ki;
	uint8_t kd;
	uint16_t lb;
	uint16_t ub;
};

void pid_init(){
	struct pid pid_roll;
	struct pid pid_pitch;
	struct pid pid_yaw;
	_set_pid_gains(&pid_roll, ROLL_KP, ROLL_KI, ROLL_KD, ROLL_LB, ROLL_UB);
	_set_pid_gains(&pid_pitch, PITCH_KP, PITCH_KI, PITCH_KD, PITCH_LB, PITCH_UB);
	_set_pid_gains(&pid_yaw, YAW_KP, YAW_KI, YAW_KD, YAW_LB, YAW_UB);
}

void _set_pid_gains(struct pid *pid_inst, uint8_t kp, uint8_t ki, uint8_t kd, int16_t lb, int16_t ub){
	pid_inst->kp = kp;
	pid_inst->ki = ki;
	pid_inst->kd = kd;
	pid_inst->lb = lb;
	pid_inst->ub = ub;
}

void pid_update(uint8_t mode, uint16_t r, uint16_t y){
	
}