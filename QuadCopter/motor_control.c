/*
 * motor_control.c
 *
 * Created: 2020-04-03 19:22:09
 *  Author: Olle
 */ 

#include "QC_incl.h"

bool glb_integrator_flag = true;

void pwm_init(){
	
	/* Set data direction of PWM ports */
	DDRD |= (1 << PD3);
	DDRD |= (1 << PD5);
	DDRD |= (1 << PD6);
	DDRB |= (1 << PB3);
	
	/* Set Timer 0 */
	TCCR0A |= (1 << COM0A1);				// OC0A to Fast PWM mode
	TCCR0A |= (1 << COM0B1);				// OC0B to Fast PWM mode
	TCCR0A |= (1 << WGM00) | (1 << WGM01);			// Fast PWM mode
	TCCR0B |= (1 << CS00) | (1 << CS01);			// P. 117 in datasheet
	
	/* Set Timer 2 */
	TCCR2A |= (1 << COM2A1);				// OC2A to Fast PWM mode
	TCCR2A |= (1 << COM2B1);				// OC2B to Fast PWM mode
	TCCR2A |= (1 << WGM20) | (1 << WGM21);			// Fast PWM mode
	TCCR2B |= (1 << CS22);					// Clock PS 64 of clkT2S (same as clkIO)
	
	pwm_set_all(90);
	_delay_ms(2000);
	pwm_set_all(60);
	_delay_ms(1000);
}

void pwm_set_fl(uint8_t u_thr, uint8_t u_roll, uint8_t u_pitch, uint8_t u_yaw){
	uint8_t duty = u_thr + u_roll + u_pitch + u_yaw;
}

void pwm_set_fr(uint8_t u_thr, uint8_t u_roll, uint8_t u_pitch, uint8_t u_yaw){
	uint8_t duty = u_thr - u_roll + u_pitch - u_yaw;
}

void pwm_set_rr(uint8_t u_thr, uint8_t u_roll, uint8_t u_pitch, uint8_t u_yaw){
	uint8_t duty = u_thr - u_roll - u_pitch - u_yaw;
}

void pwm_set_rl(uint8_t u_thr, uint8_t u_roll, uint8_t u_pitch, uint8_t u_yaw){
	uint8_t duty = u_thr + u_roll - u_pitch + u_yaw;
}

void pwm_set(uint8_t motor, uint8_t duty){
	switch(motor){
		case FL: 
		OCR2B = duty;
		break;
		case FR:
		OCR2A = duty;
		break;
		case RR:
		OCR0B = duty;
		break;
		case RL:
		OCR0A = duty;
		break;
	}
}

void pwm_set_all(uint8_t duty){
	pwm_set(FL, duty);
	pwm_set(FR, duty);
	pwm_set(RR, duty);
	pwm_set(RL, duty);
}