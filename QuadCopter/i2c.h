/*
 * i2c.h
 *
 * Created: 2020-04-02 23:27:30
 *  Author: Olle
 */ 


#ifndef I2C_H_
#define I2C_H_

#define I2C_START_COND 0x08
#define I2C_W 0
#define I2C_R 1
#define I2C_MT_ACK_REC 0x18
#define I2C_MT_NACK_REC 0x20
#define I2C_MT_ARB_LOST 0x38

#define I2C_MR_ACK_REC 0x40
#define I2C_MR_NACK_REC 0x48
#define I2C_MR_ARB_LOST 0x38

void i2c_init();

#endif /* I2C_H_ */