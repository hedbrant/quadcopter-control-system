/*
 * IMU.c
 *
 * Created: 2020-02-24 21:51:29
 *  Author: Olle
 */ 

#include "QC_incl.h"

void imu_init(){
	_imu_conf_interrupts();
	_imu_conf_samp_rate();
}

void _imu_conf_interrupts(){
	// Configure to 
	uint8_t data = (1 << 6) | (1 << 5) | (1 << 4);
	i2c_send_8_bit_data(IMU_INT_CONF_REG, data);
}

void _imu_conf_samp_rate(){
	// data = 159 for sample rate 50 Hz
	uint8_t data = 159;
	i2c_send_8_bit_data(IMU_SAMP_RATE_DIV_REG, data);
}
