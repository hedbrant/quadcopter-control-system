/*
 * system.c
 *
 * Created: 2020-04-02 23:31:01
 *  Author: Olle Hedbrant
 */ 

#include "QC_incl.h"

void system_init(){
	_set_cpu_clock();
	sei();
}

void _set_cpu_clock(){
	CLKPR = (1 << CLKPCE);
	CLKPR = (1 << CLKPS0);		// Set CPU clock freq prescaler to 4
}